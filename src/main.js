import Vue from 'vue'
import App from './App.vue'
import store from './store'
import {WEIGHT_CURRENCY} from './constants'

Vue.config.productionTip = false
Vue.prototype.WEIGHT_CURRENCY = WEIGHT_CURRENCY

new Vue({
  store,
  render: h => h(App)
}).$mount('#app')
