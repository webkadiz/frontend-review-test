import Vue from 'vue';
import Vuex from 'vuex';
import api from '../../api/products.js';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products: []
  },
  mutations: {
    setProducts(state, products) {
      state.products = products
    }
  },
  actions: {
    async getProductsList({commit}) {
      try {
        const products = await api.getProductsList();

        commit('setProducts', products)
      } catch(e) {
        // some logic, least logging, for example with Sentry
        console.log('error')
      }
    },
  },
  modules: {
  }
})
